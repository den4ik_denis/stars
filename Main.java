package com.Stars;

public class Main {
    public static void main(String[] args) {
        int[][] arr = new int [7][7];
        StarsService starsService = new StarsService();
        System.out.println("Квадрат");
        starsService.square(arr);
        System.out.println("Пустой квадрат");
        starsService.emptySquare(arr);
        System.out.println("Верхний левый треугольник");
        starsService.leftTopTriangle(arr);
        System.out.println("Нижний левый треугольник");
        starsService.leftBottomTriangle(arr);
        System.out.println("Нижний правый треугольник");
        starsService.rightBottomTriangle(arr);
        System.out.println("Верхний правый треугольник");
        starsService.rightTopTriangle(arr);
        System.out.println("В форме Х");
        starsService.theXForm(arr);
        System.out.println("Верхний малый треугольник");
        starsService.littleTopTriangle(arr);
        System.out.println("Нижний малый треугольник");
        starsService.littleBottomTriangle(arr);
    }
}
